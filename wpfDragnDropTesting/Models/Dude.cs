﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wpfDragnDropTesting.Models
{
    public class Dude
    {
        public string Name { get; set; }
        public int Age { get; set; } = 0;

        // Let's get some information lol.
        public string GetInformation 
        { 
            get
            {
                if (Age > 0)
                    return Name + $" ({Age})";
                else
                    return Name + " Teeeeeeeest";
            }
        }


        //public string GetInformation()
        //{
        //    if(Age > 0)
        //        return Name + $" ({Age})";
        //    else
        //        return Name + " Teeeeeeeest";
        //}
    }
}
