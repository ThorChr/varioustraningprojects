﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using wpfDragnDropTesting.Models;

namespace wpfDragnDropTesting.ViewModels
{
    public class EditDataViewModel : ViewModelBase
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }

        private int _age;
        public int Age
        {
            get { return _age; }
            set { _age = value; OnPropertyChanged(); }
        }

        public ListBox Source { get; set; }
        public ListBox Target { get; set; }

        public string Blah
        {
            get
            {
                string result = $"My source is: {Source.Name}, while my target is: {Target.Name}.";
                result += "\n\nNot only that, the data I contain is this: ";
                result += $"\nName: {(Source.SelectedItem as Dude).Name}, Age: {(Source.SelectedItem as Dude).Age}";
                return result;
            }
        }

        /// <summary>
        /// Constructor for EditDataViewModel. Populates properties based on parameter.
        /// </summary>
        /// <param name="sender">Should be a ListBox containing a SelectedItem.</param>
        public EditDataViewModel(ListBox data)
        {
            Name = (data.SelectedItem as Dude).Name;
            Age = (data.SelectedItem as Dude).Age;
        }
    }
}
