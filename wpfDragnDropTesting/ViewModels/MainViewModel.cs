﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using wpfDragnDropTesting.Models;

namespace wpfDragnDropTesting.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        // Lists for listboxes
        public ObservableCollection<Dude> Listbox1Col { get; set; }
        public ObservableCollection<Dude> Listbox2Col { get; set; }
        public ObservableCollection<Dude> Listbox3Col { get; set; }

        // Properties
        public Dude SelectedDude { get; set; }
        private string _nameInputValue;
        public string NameInputValue 
        { 
            get { return _nameInputValue; }
            set { _nameInputValue = value; OnPropertyChanged(); }
        }

        public ListBox Source { get; set; }
        public ListBox Target { get; set; }

        // Constructor
        public MainViewModel()
        {
            Listbox1Col = new ObservableCollection<Dude>() { 
                new Dude() { Name = "Hu Tao", Age = 20},
                new Dude() { Name = "Keqing", Age = 24},
                new Dude() { Name = "Qiqi", Age = 12},
                new Dude() { Name = "Barbara", Age = 18},
            };
            Listbox2Col = new ObservableCollection<Dude>();
            Listbox3Col = new ObservableCollection<Dude>();
        }

        // Adds NameInputValue to default list.
        public void AddToDefaultList()
        {
            if(_nameInputValue != null && _nameInputValue != "" && _nameInputValue != " ")
            {
                Dude someCoolDude = new Dude() { Name = _nameInputValue };
                Listbox1Col.Add(someCoolDude);
                NameInputValue = null;
            }
        }

        // When trying to drag something from first list to second list.
        public void DragFromListbox(object sender)
        {
            ListBox item = sender as ListBox;

            DragDrop.DoDragDrop(item, item.SelectedItem, DragDropEffects.Move);
        }

        // Handling the drop to the second list.
        public void ListboxDropHandling(DragEventArgs e, string sendersName)
        {
            Dude data = (Dude)e.Data.GetData(typeof(Dude));

            // Remove data from all the lists
            Listbox1Col.Remove(data);
            Listbox2Col.Remove(data);
            Listbox3Col.Remove(data);

            // Add data to the proper list
            switch (sendersName)
            {
                case "Listbox1":
                    Listbox1Col.Add(data);
                    break;

                case "Listbox2":
                    Listbox2Col.Add(data);
                    break;

                case "Listbox3":
                    Listbox3Col.Add(data);
                    break;
            }
        }
    }
}
