﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wpfDragnDropTesting.Models;
using wpfDragnDropTesting.ViewModels;
using wpfDragnDropTesting.Views;

namespace wpfDragnDropTesting
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainViewModel mvm;
        public MainWindow()
        {
            InitializeComponent();
            mvm = new MainViewModel();
            DataContext = mvm;

            TextboxInput.Focus();
        }

        // Upon btn click
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(TextboxInput.Text != null && TextboxInput.Text != "" && TextboxInput.Text != " ")
                mvm.AddToDefaultList();

            TextboxInput.Focus();
            TextboxInput.Clear();
        }

        // Drag handling
        private void ListboxMouseMoveHandling(object sender, MouseEventArgs e)
        {
            // Make sure sender is actually <<SOMETHING>>, and that the mouse button has been pressed.. AND THAT a selected items exists.
            if ((sender as ListBox) != null && e.LeftButton == MouseButtonState.Pressed && (sender as ListBox).SelectedItem != null)
            {
                try
                {
                    mvm.Source = (sender as ListBox); // Need this to know where we're dragging from.
                    mvm.DragFromListbox(sender);
                } catch(Exception ex)
                {
                    throw new Exception("An error occured: \n" + ex.Message);
                }
            }

        }

        // Drop handling
        private void ListboxDropHandling(object sender, DragEventArgs e)
        {
            mvm.Target = (sender as ListBox);

            // Create a new view and viewmodel.
            EditData ed = new EditData();
            EditDataViewModel edvm = new EditDataViewModel(mvm.Source);

            // Set up properties for view and viewmodel.
            ed.DataContext = edvm;
            edvm.Source = mvm.Source;
            edvm.Target = mvm.Target;

            // Show view.
            ed.ShowDialog();

            // After all is confirm, we can make drop yes
            if (ed.DialogResult == true)
            {
                mvm.ListboxDropHandling(e, (sender as ListBox).Name);
            } else
            {
                MessageBox.Show("No drop.");
            }
        }

        private void TextboxInput_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                Button_Click(sender, e);
        }

        // Handle when double clicking an item in listbox
        /*private void Listbox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                // Create the View and Viewmodel.
                EditData ed = new EditData();
                EditDataViewModel edvm = new EditDataViewModel(sender);

                // Change properties for view and viewmodel.
                ed.DataContext = edvm;

                // Show view.
                ed.ShowDialog();
            } catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }*/
    }
}
