﻿using VideoCommandbindListBox.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using VideoCommandbindListBox.Commands;
using System.Collections.ObjectModel;

namespace VideoCommandbindListBox.ViewModels
{
    public class MainViewModel
    {
        // Skal være en observableCollection for lettest at kunne ændre noget i View laget, når en ændring sker i listen.
        // Der er også en interface der kan give denne funktionalitet for en normal List, men det her er honestly lettest.
        public ObservableCollection<Person> Persons { get; set; } = new ObservableCollection<Person> 
        {
            new Person { FirstName = "Thor", LastName = "Christiansen"},
            new Person { FirstName = "Frederik", LastName = "Joost"},
            new Person { FirstName = "Benjamin", LastName = "Lissner"}
        };

        public Person SelectedPerson { get; set; }

        // Command binding
        public ICommand NewCommand { get; set; } = new NewCommand();
        public ICommand DeleteCommand { get; set; } = new DeleteCommand();
    }
}
