﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using VideoCommandbindListBox.ViewModels;

namespace VideoCommandbindListBox.Commands
{
    public class DeleteCommand : ICommand
    {
        // Den her kode tvinger ens event til at opdatere meget mere ofte.
        // Dette er for at sørge for ens CanExecute faktisk påvirker ens Delete knap til at være Disabled / Enabled
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            bool result = true;

            if(parameter is MainViewModel mvm)
            {
                if(mvm.SelectedPerson == null)
                    result = false;
            }

            CommandManager.InvalidateRequerySuggested();

            return result;
        }

        public void Execute(object parameter)
        {
            if (parameter is MainViewModel mvm)
            {
                mvm.Persons.Remove(mvm.SelectedPerson);
            }
        }
    }
}
