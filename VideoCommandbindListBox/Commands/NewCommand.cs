﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using VideoCommandbindListBox.Models;
using VideoCommandbindListBox.ViewModels;

namespace VideoCommandbindListBox.Commands
{
    public class NewCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if(parameter is MainViewModel mvm)
            {
                mvm.Persons.Add(new Person() { FirstName = "Lul", LastName = "Eksde" });
            }
        }
    }
}
