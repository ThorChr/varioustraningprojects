﻿using Microsoft.AspNetCore.Identity;

namespace TipsByAnilApiUser.Authentication
{
    public class ApplicationUser : IdentityUser
    {
    }
}
