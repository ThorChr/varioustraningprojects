﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBindingTest.Models
{
    public class Waifu
    {
        public string Name { get; set; }
        public string WeebSource { get; set; }
        public string MALLink { get; set; }
        public string PicturePath { get; set; }

        public override string ToString()
        {
            return $"{Name} [{WeebSource}]";
        }
    }
}
