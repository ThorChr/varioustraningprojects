﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace DataBindingTest.Models
{
    public class WaifuManager
    {
        public static List<Waifu> GetWaifus()
        {
            var waifus = new List<Waifu>();

            // Populate list with dummy info
            waifus.Add(new Waifu() { Name = "Zero Two", WeebSource = "Darling In The FranXX", MALLink= "https://myanimelist.net/anime/35849/Darling_in_the_FranXX", PicturePath = "Assets/ZeroReasonsToLive.png" });
            waifus.Add(new Waifu() { Name = "Yuno Gasai", WeebSource = "Future Diary", MALLink = "https://myanimelist.net/anime/10620/Mirai_Nikki", PicturePath = "Assets/YunoLookingHmmmmmmm.png" });
            waifus.Add(new Waifu() { Name = "Ayame", WeebSource = "Shimoneta", MALLink = "https://myanimelist.net/anime/29786/Shimoneta_to_Iu_Gainen_ga_Sonzai_Shinai_Taikutsu_na_Sekai", PicturePath = "Assets/Ayame.png" });

            return waifus;
        }
    }
}
