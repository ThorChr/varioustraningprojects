﻿using DataBindingTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataBindingTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Waifu> waifus;

        public MainWindow()
        {
            InitializeComponent();

            // Get our dummy information
            waifus = WaifuManager.GetWaifus();

            // Add all waifus to list
            foreach(Waifu w in waifus)
            {
                this.WaifuListView.Items.Add(w);

            }

            // Set the item source of the list view
            //WaifuListView.ItemsSource = waifus;

            // Check size of list
            WaifuListSizeText.Content = $"Waifu List Size: {waifus.Count}";
        }

        private void WaifuListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Here we get the selected object
            Waifu selectedWaifu = WaifuListView.SelectedItem as Waifu;

            // And then change text based on its values.
            TargetText.Text = $"Character: {selectedWaifu.Name} \nSource: {selectedWaifu.WeebSource} \nPicture: {selectedWaifu.PicturePath} \nMAL: {selectedWaifu.MALLink}";
        }

        private void NewWaifuAdder_Click(object sender, RoutedEventArgs e)
        {
            waifus.Add(new Waifu() { Name = "Rui Tachibana", WeebSource = "Domestic Girlfriend", MALLink = "https://myanimelist.net/anime/37982/Domestic_na_Kanojo", PicturePath = "RuiSadLookingSide.png" });

            // Add the newest waifu to list
            //WaifuListView.Items.Add(waifus[count - 1]);
            //WaifuListView.ItemsSource = waifus;

            this.WaifuListView.Items.Add(new Waifu() { Name = "Rui Tachibana", WeebSource = "Domestic Girlfriend", MALLink = "https://myanimelist.net/anime/37982/Domestic_na_Kanojo", PicturePath = "RuiSadLookingSide.png" });

            // Check size of list
            WaifuListSizeText.Content = $"Waifu List Size: {waifus.Count}";
        }
    }
}
