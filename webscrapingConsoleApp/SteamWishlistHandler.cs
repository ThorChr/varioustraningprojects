﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace webscrapingConsoleApp
{
    public class SteamWishlistHandler
    {
        public string Username { get; init; }

        public SteamWishlistHandler(string username)
        {
            Username = username;
        }

        public async Task<string> GetWishlistHtml()
        {
            string url = "https://store.steampowered.com/wishlist/id/" + Username;

            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls13;
            client.DefaultRequestHeaders.Accept.Clear();
            var response = await client.GetStringAsync(url);

            return response;
        }

        public async Task<IEnumerable<string>> FindAllGames(string html)
        {
            HtmlDocument htmlDoc = new();
            htmlDoc.LoadHtml(html);

            var games = htmlDoc.DocumentNode.Descendants("div").Where(node => node.GetAttributeValue("class", "").Contains("wishlist_row"));

            return (IEnumerable<string>)games;
        }
    }
}
