﻿using System;

namespace FirewallHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("FirewallHandling.\n\tLet's try to figure this shit out.\n\n");

            // Create new instance of the class.
            FirewallHandler fw = new FirewallHandler();

            // Set up variables.
            string ruleName = "Soloenabler Testing";
            string portRangeToBlock = "27000-27200,3097";


            // Check if FW rule exists
            if(fw.DoesFWRuleExist(ruleName)) 
            { 
                Console.WriteLine("Rule already exists.");
                fw.RemoveFirewallRule(ruleName);
            }
            else {
                Console.WriteLine("Rule does not exist. Creating.");

                // Name. Portrange. Outbound yes/no. UDP yes/no.
                fw.CreateFWRule(ruleName: ruleName, portValue: portRangeToBlock, isOut: true, isUDP: true);
                fw.CreateFWRule(ruleName: ruleName, portValue: portRangeToBlock, isOut: true, isUDP: false);
                fw.CreateFWRule(ruleName: ruleName, portValue: portRangeToBlock, isOut: false, isUDP: true);
                fw.CreateFWRule(ruleName: ruleName, portValue: portRangeToBlock, isOut: false, isUDP: false);
            }


            Console.ReadKey();
        }
    }
}
