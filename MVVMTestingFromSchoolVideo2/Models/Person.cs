﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMTestingFromSchoolVideo2.Models
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
