﻿using MVVMTestingFromSchoolVideo2.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMTestingFromSchoolVideo2.ViewModels
{
    public class MainViewModel
    {
        public List<Person> Persons { get; set; } = new List<Person> 
        {
            new Person { FirstName = "Thor", LastName = "Christiansen"},
            new Person { FirstName = "Frederik", LastName = "Joost"},
            new Person { FirstName = "Benjamin", LastName = "Lissner"}
        };

        public Person SelectedPerson { get; set; }
    }
}
