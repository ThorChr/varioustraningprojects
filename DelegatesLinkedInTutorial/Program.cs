﻿using System;

namespace DelegatesLinkedInTutorial
{
    // Declare the delecate
    public delegate string myDel(int arg1, int arg2);
    public delegate void myDelRef(int arg1, ref int arg2);

    // Can also declare it as an instanceMethod
    class MyClass
    {
        public string instanceMethod1(int a, int b)
        {
            return ((a + b) * a).ToString();
        }
    }

    class Program
    {
        // Declare delegate functions
        static string func1(int a, int b)
        {
            return (a + b).ToString();
        }

        static string func2(int a, int b)
        {
            return (a * b).ToString();
        }

        static void Main(string[] args)
        {
            myDel f = func1;
            Console.WriteLine($"The number is: {f(10, 20)}");
            f = func2;
            Console.WriteLine($"The number is: {f(10, 20)}");

            // Create instance of class
            MyClass mc = new MyClass();
            f = mc.instanceMethod1;
            Console.WriteLine($"The number is: {f(10, 20)}");

            // Can also do "anonymous delegates"... Basically create a delegate where we'll use it.
            myDel d = delegate (int a, int b)
            {
                return (a + b - a * b / a).ToString();
            };

            Console.WriteLine($"The number is: {d(10, 20)}");

            // Let's try working with reference values
            Console.WriteLine("\nLet's start with 2 numbers: \n\ta: 10\n\tb: 5\n");

            int a = 10;
            int b = 5;

            static void refFunc1(int arg1, ref int arg2)
            {
                string result = (arg1 + arg2).ToString();
                arg2 *= 2;
                Console.WriteLine("The result is: " + result);
            }

            static void refFunc2(int arg1, ref int arg2)
            {
                string result = (arg1 * arg2).ToString();
                Console.WriteLine("The result is: " + result);
            }

            Console.WriteLine($"Value of b {b}");
            refFunc1(a, ref b);
            Console.WriteLine($"Value of b {b}");
            refFunc2(a, ref b);
            Console.WriteLine($"Value of b {b}");
        }
    }
}
