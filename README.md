# Repo containing various training projects.
All projects will probably be half-baked. Most won't be functional.
The sole purpose of this repo, is to contain some projects where I mess around with C# and stuff relating to it, in order to learn how stuff works.

## Links to useful projects

#### FileHandling.
Makes the handling of text files a lot easier. <br>
<b>Link: </b>https://gitlab.com/ThorChr/varioustraningprojects/-/tree/master/FileHandling

#### FirewallHandling.
Makes the handling of the Windows Firewall somewhat easier. 

It's still a bit limited in features, but supports checking to see if a rule exists, can add and delete rules.<br>
<b>Link: </b>https://gitlab.com/ThorChr/varioustraningprojects/-/tree/master/FirewallHandling

## Ending note

Feel free to create an issue if something should be changed, or even create a commit yourself.