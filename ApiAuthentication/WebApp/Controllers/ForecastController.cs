﻿using Microsoft.AspNetCore.Mvc;
using SharedProject;
using SharedProject.Models;
using System.Net.Http.Headers;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [Route("[controller]")]
    public class ForecastController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _apiUrl = "https://localhost:7120/api/";

        public ForecastController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        
        public async Task<IActionResult> Index()
        {
            var forecastVM = new ForecastViewModel();

            // Create request
            var request = new HttpRequestMessage(HttpMethod.Get, _apiUrl + "WeatherForecast");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Request.Cookies[Constants.XAccessToken]);

            // Send
            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request);

            // Recieve response
            if(response.IsSuccessStatusCode)
            {
                // Expect a list of weatherforecast.
                var jsonObject = HelperMethods.Deserialize<List<WeatherForecast>>(await response.Content.ReadAsStringAsync());

                forecastVM.Forecast = jsonObject;
            } else
            {
                var reasonPhrase = response.ReasonPhrase;
                var responseModel = new ResponseModel
                {
                    Status = reasonPhrase,
                    Message = (reasonPhrase == "Unauthorized") ? "You need to be logged in to access this content." : "You don't have the required permissions to access this content."
                };

                forecastVM.Response = responseModel;
            }


            return View(forecastVM);
        }
    }
}
