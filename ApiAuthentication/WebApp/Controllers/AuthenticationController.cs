﻿using Microsoft.AspNetCore.Mvc;
using SharedProject;
using SharedProject.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly string _apiUrl = "https://localhost:7120/api/";

        public AuthenticationController(IHttpClientFactory clientFactory)
        {
            _httpClientFactory = clientFactory;
        }

        [HttpGet]
        [Route("Register")]
        public async Task<IActionResult> Register()
        {
            // So far, this one can be quite basic. This method only needs to show the view to the user.
            // Could be upgraded upon in the future.

            return View(new RegisterViewModel());
        }

        [HttpGet]
        [Route("Login")]
        public async Task<IActionResult> Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterViewModel modelVM)
        {
            RegisterModel model = modelVM.RegisterModel;

            // Create the request.
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, _apiUrl + "Authentication/Register");
            request.Content = new StringContent(JsonSerializer.Serialize(model));
            request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            // Create client and send request.
            HttpClient client = _httpClientFactory.CreateClient();
            HttpResponseMessage response = await client.SendAsync(request);

            // Handle the reply from the API.
            var responseObject = HelperMethods.Deserialize<ResponseModel>(await response.Content.ReadAsStringAsync());
            modelVM.Response = responseObject;

            return View(modelVM);
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginViewModel modelVM)
        {
            LoginModel model = modelVM.LoginModel;

            // Create request.
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, _apiUrl + "Authentication/Login");
            request.Content = new StringContent(JsonSerializer.Serialize(model));
            request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            // Create client and send request.
            HttpClient client = _httpClientFactory.CreateClient();
            HttpResponseMessage response = await client.SendAsync(request);

            // Check for success and whatnot.
            if (response.IsSuccessStatusCode)
            {
                var responseFromJson = HelperMethods.Deserialize<ApiTokenLoginModel>(await response.Content.ReadAsStringAsync());
                //var responseFromJson = Deserialize<ApiTokenLoginModel>(await response.Content.ReadAsStringAsync());

                // Create a cookie so we can store the token.
                Response.Cookies.Append(
                    Constants.XAccessToken, 
                    responseFromJson.Token, 
                    new CookieOptions 
                    {
                        HttpOnly = true,
                        SameSite = SameSiteMode.Strict
                    });

                Response.Cookies.Append(
                    Constants.XAccessTokenExpiration,
                    responseFromJson.Expiration.ToString(),
                    new CookieOptions
                    {
                        HttpOnly = true,
                        SameSite = SameSiteMode.Strict
                    });

                return RedirectToAction(controllerName: "Home", actionName: "index");
            } else
            {
                var responseFromJson = HelperMethods.Deserialize<ResponseModel>(await response.Content.ReadAsStringAsync());
                modelVM.Response = responseFromJson;

                return View(modelVM);
            }
        }

        [HttpGet]
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            Response.Cookies.Delete(Constants.XAccessToken);
            Response.Cookies.Delete(Constants.XAccessTokenExpiration);

            return RedirectToAction(controllerName: "Home", actionName: "index");
        }
    }
}
