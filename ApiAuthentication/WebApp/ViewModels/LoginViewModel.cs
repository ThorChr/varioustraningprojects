﻿using SharedProject.Models;

namespace WebApp.ViewModels
{
    public class LoginViewModel
    {
        public LoginModel LoginModel { get; set; }
        public ResponseModel? Response { get; set; }
    }
}
