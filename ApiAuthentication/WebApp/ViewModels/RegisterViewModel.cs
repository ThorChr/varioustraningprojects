﻿using SharedProject.Models;

namespace WebApp.ViewModels
{
    public class RegisterViewModel
    {
        public RegisterModel RegisterModel { get; set; }
        public ResponseModel? Response { get; set; }
    }
}
