﻿using SharedProject.Models;

namespace WebApp.ViewModels
{
    public class ForecastViewModel
    {
        public List<WeatherForecast>? Forecast { get; set; }
        public ResponseModel? Response { get; set; }
    }
}
