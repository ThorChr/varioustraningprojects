﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace SharedProject
{
    public static class HelperMethods
    {
        /// <summary>
        /// Short method, used for deserializing some json to a specific datatype.
        /// </summary>
        /// <typeparam name="T">The datatype the JSON should be deserialized into.</typeparam>
        /// <param name="jsonString">Type: Stream. Contains the json to be deserialized.</param>
        /// <returns></returns>
        public static T Deserialize<T>(string jsonString)
        {
            var options = new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true
            };
            var response = JsonSerializer.Deserialize<T>(jsonString, options);

            return response;
        }
    }
}
