﻿using Microsoft.AspNetCore.Identity;

namespace APIProject.Authentication
{
    public class ApplicationUser : IdentityUser
    {
    }
}
