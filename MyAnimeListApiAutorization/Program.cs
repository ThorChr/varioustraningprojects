﻿using System;
using System.Linq;
using System.Net.Http;

namespace MyAnimeListApiAutorization
{
    class Program
    {
        // HttpClient is intended to be instantiated once per application, rather than per-use. See Remarks.
        static readonly HttpClient client = new HttpClient();

        static Random random = new Random();

        static void Main(string[] args)
        {
            Console.WriteLine(RandomString(64));
        }

         
        /// <summary>
        /// Used for generating a random string of characters to a given length.
        /// </summary>
        /// <param name="length">Length of thing to generate.</param>
        /// <returns></returns>
        static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
