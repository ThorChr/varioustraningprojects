﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandlingInFolders
{
    public class FileHandler
    {
        public string Path { get; set; }

        public FileHandler(string path)
        {
            Path = path;
        }

        /// <summary>
        /// Gets a path to each folder there exists in the given path.
        /// </summary>
        /// <returns></returns>
        public string[] GetAllMainFolders()
        {
            string[] dirs = Directory.GetDirectories(Path);
            return dirs;
        }

        /// <summary>
        /// Gets a path to each file there exists at a specified path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string[] GetAllFilesInFolder(string path)
        {
            string[] files = Directory.GetFiles(path);
            return files;
        }

        /// <summary>
        /// Remove junk from the filename at the specified path.
        /// By junk, I mean [this stuff].
        /// </summary>
        /// <param name="path"></param>
        /// <returns>A filepath with a clean name.</returns>
        public string RemoveJunkInFileName(string path)
        {
            // Break the original string into an array so we can work with each element.
            string[] pathBrokenIntoArray = path.Split('\\');
            int lengthOfArray = pathBrokenIntoArray.Length;
            string originalFileName = pathBrokenIntoArray[lengthOfArray - 1];

            // Find out which part of the name we need to remove.
            int indexOfStartSquareBracket;
            int indexOfEndSquareBracket;
            int difference;

            do
            {
                indexOfStartSquareBracket = originalFileName.IndexOf('[');
                indexOfEndSquareBracket = originalFileName.IndexOf(']');
                difference = (indexOfEndSquareBracket) - (indexOfStartSquareBracket) + 1;

                if(indexOfStartSquareBracket >= 0)
                {
                    // Remove that part of the name.
                    string newName = originalFileName.Remove(indexOfStartSquareBracket, difference).Trim(' ');
                    originalFileName = newName;
                }

            } while (indexOfEndSquareBracket > -1);

            // If there is a space in front of the file extension, then remove that space.
            if(originalFileName[originalFileName.Length - 5] == ' ')
                originalFileName = originalFileName.Remove(originalFileName.Length - 5, 1);

            // Replace the old name with the new one.
            pathBrokenIntoArray[lengthOfArray - 1] = originalFileName;

            // Create a new name and path.
            string newNameWithFilePath = "";
            foreach(string element in pathBrokenIntoArray)
                newNameWithFilePath += element + "\\";

            newNameWithFilePath = newNameWithFilePath.Trim('\\');

            // In the end, return the result.
            return newNameWithFilePath;
        } 

        /// <summary>
        /// Renames a file, so it matches up with the folder name given.
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="fileName"></param>
        /// <returns>e.g. foldername - Episode 01.mp4</returns>
        public string FolderFileNameRelation(string folderPath, string fileName)
        {
            List<string> episodePrefixes = new List<string>();
            episodePrefixes.Add("episode");
            episodePrefixes.Add("ep");
            episodePrefixes.Add("e");

            List<int> episodeSuffixes = new List<int>();
            for (int i = 1; i < 25; i++)
                episodeSuffixes.Add(i);

            string episodeNumberString = "";
            string extensionOfFile = fileName.Substring(fileName.Length - 4, 4);

            for (int i = 0; i < episodePrefixes.Count; i++)
            {

                for(int j = 0; j < episodeSuffixes.Count; j++)
                {

                    string combined = $"{episodePrefixes[i]}{episodeSuffixes[j]}";
                    string combinedExtraZero = $"{episodePrefixes[i]}0{episodeSuffixes[j]}";
                    string combinedWithSpace = $"{episodePrefixes[i]} {episodeSuffixes[j]}";

                    // We have a match.
                    if(fileName.ToLower().Contains(combined) || fileName.ToLower().Contains(combinedWithSpace) || fileName.ToLower().Contains(combinedExtraZero))
                    {
                        
                        // Now we need to figure out if the episode number is less than 10, and if so, then add a 0 in front.
                        if(episodeSuffixes[j] < 10)
                        {
                            string episodeSuffixToUse = "0";
                            episodeSuffixToUse += episodeSuffixes[j].ToString();

                            episodeNumberString = "Episode " + episodeSuffixToUse;
                        } else
                        {
                            episodeNumberString = "Episode " + episodeSuffixes[j];
                        }
                        
                    }

                }

            }

            string[] folderPathToArray = folderPath.Split('\\');

            string result = $"{folderPath}\\{folderPathToArray[folderPathToArray.Length - 1]} - {episodeNumberString}{extensionOfFile}";

            return result;
        }

        /// <summary>
        /// Renames a given file with a new name.
        /// </summary>
        /// <param name="originalPath"></param>
        /// <param name="newPath"></param>
        private void RenameFile(string originalPath, string newPath)
        {
            Console.WriteLine($"Original Path: {originalPath}");
            Console.WriteLine($"New Path: {newPath}");

            if(originalPath != newPath)
                Directory.Move(@originalPath, @newPath);

            //File.Move(originalPath, newPath);
        }

        /// <summary>
        /// Renames the folders in the supplied path.
        /// </summary>
        public void RenameFolders()
        {
            // Get all directories in the path.
            var dirs = GetAllMainFolders();
            Console.WriteLine($"Total folders found: {dirs.Length}");

            // Go through them all.
            foreach (string @dir in dirs)
            {
                // Create a new name.
                string newName = @RemoveJunkInFileName(dir);
                Console.WriteLine($"\tCleaned up dir name.");

                // Give them a new name.
                RenameFile(dir, newName);
                Console.WriteLine($"Renamed directory.");
            }
        }

        /// <summary>
        /// Renames the files within the folders in the path.
        /// </summary>
        public void RenameFiles(bool UsePathSpecifiedByProperty)
        {
            if(!UsePathSpecifiedByProperty)
            {
                // Get all directories.
                var dirs = GetAllMainFolders();

                // Go through them all.
                foreach (string @dir in dirs)
                {
                    // Get all files in directory.
                    var files = GetAllFilesInFolder(@dir);

                    // Go through all files.
                    foreach (string @file in files)
                    {
                        string cleanName = FolderFileNameRelation(dir, file);
                        RenameFile(@file, @cleanName);
                        Console.WriteLine("Renamed file.");
                    }
                }
            } else
            {
                // Get all files in directory.
                var files = GetAllFilesInFolder(Path);
                Console.WriteLine($"Files found: {files.Length}");

                // Go through all files.
                foreach (string @file in files)
                {
                    string cleanName = FolderFileNameRelation(Path, file);
                    if(cleanName.Contains("episode"))
                    {
                        RenameFile(@file, @cleanName);
                        Console.WriteLine("Renamed file.");
                    }
                    
                }
            }
            
        }
    }
}
