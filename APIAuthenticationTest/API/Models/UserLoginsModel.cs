﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class UserLoginsModel
    {
        [Required]
        public string Username { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;

        // Empty constructor because Indian guy said so.
        public UserLoginsModel() {}
    }
}
