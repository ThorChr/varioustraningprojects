﻿using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountController : ControllerBase
    {
        private readonly JwtSettings jwtSettings;
        public AccountController(JwtSettings jwtSettings)
        {
            this.jwtSettings = jwtSettings;
        }

        private IEnumerable<UserModel> logins = new List<UserModel>() {
            new UserModel() {
                    Id = Guid.NewGuid(),
                        EmailId = "adminakp@gmail.com",
                        Username = "Admin",
                        Password = "Admin",
                },
                new UserModel() {
                    Id = Guid.NewGuid(),
                        EmailId = "adminakp@gmail.com",
                        Username = "User1",
                        Password = "Admin",
                }
        };

        [HttpPost]
        public IActionResult GetToken(UserLoginsModel userLogins)
        {
            try
            {
                var Token = new UserTokens();

                // If user data matches.
                var Valid = logins.Any(x => x.Username.Equals(userLogins.Username, StringComparison.OrdinalIgnoreCase));
                if (Valid)
                {
                    var user = logins.FirstOrDefault(x => x.Username.Equals(userLogins.Username, StringComparison.OrdinalIgnoreCase));
                    Token = JwtHelpers.JwtHelpers.GenTokenkey(new UserTokens()
                    {
                        EmailId = user.EmailId,
                        GuidId = Guid.NewGuid(),
                        UserName = user.Username,
                        Id = user.Id,
                    }, jwtSettings);
                }
                else
                {
                    return BadRequest($"wrong password");
                }
                return Ok(Token);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Get List of UserAccounts
        /// </summary>
        /// <returns>List Of UserAccounts</returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetList()
        {
            return Ok(logins);
        }
    }

}
