﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            // Values we need to get from user
            int rowNum = 226;               // Kombination af hal + reol nummer.
            int fag = 12;                   // Ehh, "fag" but in Danish!
            int firstTwoDigits = 07;        // Hyldenummer
            int lastTwoDigits = 02;         // Inddeling
            
            // Calculate the first digit of the checkciffer
            int firstDigLarge = lastTwoDigits + (firstTwoDigits * fag) + rowNum;
            int firstDig = firstDigLarge % 8;

            // Calculate the last digit of the checkciffer
            int secondDigLarge = lastTwoDigits + (firstTwoDigits *fag) + rowNum;
            int secondDigDiv = secondDigLarge / 9;
            int secondDigMul = secondDigDiv * 9;
            int secondDig = firstDigLarge - secondDigMul;

            Console.WriteLine($"{firstDig}{secondDig}");*/

            int duration = 61;
            int hour = 60;
            int PricePrHour = 875;

            // Get hour count, rounds down.
            int hourCount = duration / hour;

            // Check if we have started a new hour
            if (duration % hour > 0)
                hourCount++;

            int price = PricePrHour * hourCount;

            Console.WriteLine($"Hours: {hourCount}, which costs: {price}");
        }
    }
}
