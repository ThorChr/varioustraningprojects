﻿using System;

namespace VideoEventsVsDelegates
{
    class Program
    {
        static void Main(string[] args)
        {
            // News publisher
            Publisher p = new Publisher("UCL");

            // News watcher
            Subscriber s1 = new Subscriber("S1");
            Subscriber s2 = new Subscriber("S2");
            Subscriber s3 = new Subscriber("S3");

            // Interconnect the methods from subscriber to the delegate defines in publisher
            /* 
             * Årsagen til at det her gøres, er at hvis man så kalder på "NewsPublished" vil alle
             * metoder der er koblet til denne delegate også blive kaldt. 
             * Dette betyder at man let kan få adskillige metoder til at blive kørt, fra andre klasser
             * uden særligt meget arbejde (som kan ses i dette eksempel). 
             */
            p.NewsPublished += s1.NewsReceived;
            p.NewsPublished += s2.NewsReceived;
            p.NewsPublished += s3.NewsReceived;

            /* Event vs Delegate forklaring.
             * Vi har lavet vores delegate om til et event. Betydningen af dette er:
             * - Vi kan nu beskytte vores subscribers til det event. Før var det muligt at sige:
                p.NewsPublished = s3.NewsReceived;
             *  hvilket ville have fjernet s1 og s2 som subscribers til vores delegate, hvilket betyder kun s3 ville få nyhedsopdateringen.
             *  Dette kan man nu ikke længere gøre.

             * - Derudover var det før muligt at gøre dette:
                p.NewsPublished("Kald på delegate");
             *  Hvilket betyder vi ville meddele alle subscribers om en ny nyhed, uden at der var kommet en nyhed. Dette er også fikset med events.
             */

            // Create some news
            p.News = "Update: Covid still sucks.";

            p.NewsPublished -= s3.NewsReceived; // Remove s3, cuz he's Covid denier

            p.News = "Another update: Just checked, and yup, Covid still sucks.";

            // NOTE: Et event er stadigt en delegate. Det er dog en form for beskyttelse til brugen af ens delegate. Timestamp: 23:30 i dagens video.
        }
    }
}
