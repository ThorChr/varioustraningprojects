﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoEventsVsDelegates
{
    // Det her er en delegate type. Det er ikke en delegate som så.. Så den kan IKKE blive instantieret / refereret til.
    // Den her linje siger kun, at vi andre steder kan lave en delegate der har samme signatur som defineret her.
    public delegate void NewsHandler(string news);

    public class Publisher
    {
        // Dette er nu vores backing variabel for vores event.
        // Vi har brug for denne, fordi vi har tilføjet add{} og remove{} til vores event.
        private NewsHandler _newsPublished;

        // Vi har nu omdefineret vores tidligere delegate variabel, til nu at være et event. 
        // Det gode ved et event, er at man ikke kan kalde på et event ude fra en anden klasse. 
        // Derudover kan man heller ikke sætte den til at være = noget bestemt, man kan dog stadigt koble ting til eventet +=
        public event NewsHandler NewsPublished
        {
            // Ligesom man ved properties har get{} og set{} har man det samme ved events
            // dog med den forskel, at de hedder "adder" og "remover", eller skrevet "add" og "remove".
            // En forskel fra properties her, er at man SKAL have begge to med. Ved properties kan man nøjes med get {}, 
            // dette er dog IKKE en mulighed her.
            // Ved at vi gør det her, så overtager vi styringen af det her event. Det betyder at vi SKAL have en "backing variabel".
            // Denne backing variabel er hvad vores event interagerer med (se lidt på det som en "full property").
            add 
            {
                _newsPublished += value;
                Console.WriteLine("Subscriber added.");
            }
            remove 
            {
                _newsPublished -= value;
                Console.WriteLine("Subscriber removed.");
            }
        }

        public string Name { get; }
        private string _news;

        public string News
        {
            get { return _news; }
            set { 
                if(_news != value)
                {
                    _news = value;

                    // Vi har fået os en ny nyhed, hvilket betyder vi skal gøre programmet opmærksom på dette.
                    // Dette gør vi ved at kalde på vores delegate NewsPublished, som vi definerede på linje 15.
                    // Hvad der sker, er basically at alle metoder der er koblet til denne delegate, også vil blive kaldt.
                    // Så ved at kalde på vores delegate i den her class, kan vi næsten pr. automatik kalde på andre metoder i andre klasser
                    // så længe at man husker at koble de andre metoder til vores delegate (hvilket vi netop gør ude i Program.cs).
                    OnNewsPublished();
                }
            }
        }

        /// <summary>
        /// Constructor for Publisher class. 
        /// </summary>
        /// <param name="name">The name of the publisher object.</param>
        public Publisher(string name)
        {
            Name = name;
        }

        /// <summary>
        /// When a new story is released, we'll use this to make use of our delegate, to "tell" other classes.
        /// </summary>
        private void OnNewsPublished()
        {
            // Årsagen til at koden her er i en metode for sig selv, er grundet "best practice".
            // Programmet ville fungere fint hvis man ikke havde den her metode, og dens kode istedet direkte stod på linje 56.
            // Men Microsoft har erklæret at den her kode stil, er "best practice", så man kan lige så godt følge det.
            if (_newsPublished != null)
                _newsPublished(_news);
        }
    }
}
