﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoEventsVsDelegates_OnlyDelegates
{
    // Det her er en delegate type. Det er ikke en delegate som så.. Så den kan IKKE blive instantieret / refereret til.
    // Den her linje siger kun, at vi andre steder kan lave en delegate der har samme signatur som defineret her.
    public delegate void NewsHandler(string news);

    public class Publisher
    {
        // Det her er en delegate variabel, som har typen "NewsHandler" tidligere defineret.
        // Den her kan man tilkoble andre metoder som har samme signatur som beskrevet på linje 9.
        public NewsHandler NewsPublished;

        public string Name { get; }
        private string _news;

        public string News
        {
            get { return _news; }
            set { 
                if(_news != value)
                {
                    _news = value;

                    // Vi har fået os en ny nyhed, hvilket betyder vi skal gøre programmet opmærksom på dette.
                    // Dette gør vi ved at kalde på vores delegate NewsPublished, som vi definerede på linje 15.
                    // Hvad der sker, er basically at alle metoder der er koblet til denne delegate, også vil blive kaldt.
                    // Så ved at kalde på vores delegate i den her class, kan vi næsten pr. automatik kalde på andre metoder i andre klasser
                    // så længe at man husker at koble de andre metoder til vores delegate (hvilket vi netop gør ude i Program.cs).
                    OnNewsPublished();
                }
            }
        }

        /// <summary>
        /// Constructor for Publisher class. 
        /// </summary>
        /// <param name="name">The name of the publisher object.</param>
        public Publisher(string name)
        {
            Name = name;
        }

        /// <summary>
        /// When a new story is released, we'll use this to make use of our delegate, to "tell" other classes.
        /// </summary>
        private void OnNewsPublished()
        {
            // Årsagen til at koden her er i en metode for sig selv, er grundet "best practice".
            // Programmet ville fungere fint hvis man ikke havde den her metode, og dens kode istedet direkte stod på linje 33.
            // Men Microsoft har erklæret at den her kode stil, er "best practice", så man kan lige så godt følge det.
            if (NewsPublished != null)
                NewsPublished(_news);
        }
    }
}
