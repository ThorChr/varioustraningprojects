﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoEventsVsDelegates_OnlyDelegates
{
    public class Subscriber
    {
        public string Name { get; }

        /// <summary>
        /// Constructor for Subscriber
        /// </summary>
        /// <param name="name">The name of the subscriber object.</param>
        public Subscriber(string name)
        {
            Name = name;
        }

        // Signaturen på den her metode skal matche signaturen på vores erklærede delegate.
        // Dette er fordi vi ønsker at koble den her metode til vores delegate.
        public void NewsReceived(string news)
        {
            Console.WriteLine($"Subscriber {Name} has received the news \"{news}\"");
        }
    }
}
