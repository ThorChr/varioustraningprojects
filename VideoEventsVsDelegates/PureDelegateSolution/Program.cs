﻿using System;

namespace VideoEventsVsDelegates_OnlyDelegates
{
    class Program
    {
        static void No(string[] args)
        {
            // News publisher
            Publisher p = new Publisher("UCL");

            // News watcher
            Subscriber s1 = new Subscriber("S1");
            Subscriber s2 = new Subscriber("S2");
            Subscriber s3 = new Subscriber("S3");

            // Interconnect the methods from subscriber to the delegate defines in publisher
            /* 
             * Årsagen til at det her gøres, er at hvis man så kalder på "NewsPublished" vil alle
             * metoder der er koblet til denne delegate også blive kaldt. 
             * Dette betyder at man let kan få adskillige metoder til at blive kørt, fra andre klasser
             * uden særligt meget arbejde (som kan ses i dette eksempel). 
             */
            p.NewsPublished += s1.NewsReceived;
            p.NewsPublished += s2.NewsReceived;
            p.NewsPublished += s3.NewsReceived;

            // Create some news
            p.News = "Update: Covid still sucks.";
            p.News = "Another update: Just checked, and yup, Covid still sucks.";

        }
    }
}
