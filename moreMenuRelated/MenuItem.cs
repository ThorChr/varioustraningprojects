﻿using System;
using System.Collections.Generic;
using System.Text;

namespace moreMenuRelated
{
    class MenuItem
    {
        private string _title;
        public string Title {
            get { return _title; }
            set { _title = value; } 
        }

        public MenuItem(string title)
        {
            _title = title;
        }
    }
}
