﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HYDAC
{
    class Controller
    {
        private Employee[] employees;
        private Employee selectedEmployee;
        private Guest selectedGuest;
        private MeetingRoom[] meetingRooms;
        private MeetingRoom selectedMeetingRoom;

        public bool SelectEmployee(string name)
        {
            return true; // Should be changed. This is just to make sure the method won't give an error.
        }

        public void RegisterGuest(string name, string phone, string mail)
        {

        }

        public bool SelectMeetingRoom(string name)
        {
            return true; // Should be changed. This is just to make sure the method won't give an error.
        }

        // There needs to be an overview... But no methods exists for that yet, so we need to create one.
        public void Overview()
        {
            string path = "workers.txt";    // Could probably just be written directly into the object call.. but whatev
            StreamReader sr = new StreamReader(path);

            Console.WriteLine("Medarbejder oversigt.\n");
            while (!sr.EndOfStream)
            {
                // Split worker info into array, based on comma seperation
                string[] workerInfo = sr.ReadLine().Split(',');

                Console.WriteLine("\t{0:15} Humør: {1:10}", workerInfo[0], workerInfo[1]);
            }

            Console.WriteLine("\n\n0 for at gå tilbage.");
        }
    }
}
