﻿using HYDAC;
using System;

namespace moreMenuRelated
{
    class Program
    {
        static void Main(string[] args)
        {
            // Controller thing
            Controller controller = new Controller();

            // main menu stuff
            Menu mainMenu = new Menu("Main menu", 2);
            mainMenu.AddMenuItem("1) Medarbejder");
            mainMenu.AddMenuItem("2) Gæst");

            // Loop to make program repeat constantly
            while (true)
            {
                mainMenu.Show(false);

                // Find out what the fuck user wants
                int userMenuChoice = mainMenu.SelectMenuItem();
                switch (userMenuChoice)
                {
                    case 1:
                        Menu workerMenu = new Menu("Medarbejder: \n", 5);
                        workerMenu.AddMenuItem("1. Mød ind.");
                        workerMenu.AddMenuItem("2. Forlad bygning.");
                        workerMenu.AddMenuItem("3. Inviter gæst.");
                        workerMenu.AddMenuItem("4. Tjek gæst ud.");
                        workerMenu.AddMenuItem("5. Tilstedeværelses oversigt.");

                        while (true)
                        {
                            workerMenu.Show();

                            int userWorkerMenuChoice = workerMenu.SelectMenuItem();

                            // If user wants to get out of loop, back to Worker menu
                            if (userWorkerMenuChoice == 0)
                                break;

                            // Well, a switch inside a switch.. is this bad practice? No idea tbh... I mean, one MIGHT be able to make it so this is all we need.
                            switch (userWorkerMenuChoice)
                            {
                                case 1:
                                    // Call various classes / methods here maybe?
                                    break;

                                case 5:
                                    while (true)
                                    {
                                        Console.Clear();
                                        controller.Overview();

                                        // Get user choice.
                                        int userWorkerChoice = workerMenu.SelectMenuItem();

                                        // If user wants to get out of loop, back to Worker menu
                                        if (userWorkerChoice == 0)
                                            break;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 2:
                        Console.WriteLine("inside 2");
                        break;
                    default:
                        Console.WriteLine("eksde");
                        break;
                }
            }
        }
    }
}
