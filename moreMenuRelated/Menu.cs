﻿using System;
using System.Collections.Generic;
using System.Text;

namespace moreMenuRelated
{
    class Menu
    {
        private string _title;
        private MenuItem[] _menuItems;
        private int _itemCount = 0;

        public Menu(string title, int arrCount)
        {
            _title = title;
            _menuItems = new MenuItem[arrCount];
        }

        public void Show(bool back = true)
        {
            Console.Clear();
            Console.WriteLine(_title);
            foreach(MenuItem item in _menuItems)
            {
                Console.WriteLine("\t{0}",item.Title);
            }

            // If we need to display the back button...
            if(back)
                Console.WriteLine("\n\nTryk 0 for at gå tilbage.");
        }

        // Add new menu item. 
        public void AddMenuItem(string mT)
        {
            // Initialize new object. No checks for array length etc
            MenuItem mi = new MenuItem(mT);
            _menuItems[_itemCount] = mi;
            _itemCount++;
        }

        // Select whatev menu item user wants.. Some code inside here (to check if input is int) could be placed inside a method for repetition. However, not allowed.
        public int SelectMenuItem()
        {
            // Check if input is int
            int mUV = 0;
            while (!int.TryParse(Console.ReadLine(), out mUV))
            {
                Console.Write("\nOnly use integers: ");
            }

            // Check if input is in a valid range
            while (mUV < 0 || mUV > _itemCount)
            {
                Console.Write("\nIkke et validt valg, forsøg igen.");

                // Check if input is int
                mUV = 0;
                while (!int.TryParse(Console.ReadLine(), out mUV))
                {
                    Console.Write("\nOnly use integers: ");
                }
            }

            return mUV;
        }

    }
}
