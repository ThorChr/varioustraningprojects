﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HYDAC
{
    class Guest
    { // git test
        private string name;
        private string phone;
        private string mail;
        private bool present;

        // Constructor
        public Guest(string name, string phone, string mail)
        {
            this.name = name;
            this.phone = phone;
            this.mail = mail;
        }

        // Method for handling presence
        public void SetPresence(bool present)
        {
            this.present = present;
        }
    }
}
