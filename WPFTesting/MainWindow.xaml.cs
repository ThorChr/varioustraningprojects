﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFTesting
{
    /// <summary>
    /// 
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<TodoItem> _names;
        public MainWindow()
        {
            InitializeComponent();


            _names = new List<TodoItem>();
            _names.Add(new TodoItem("Zero Two"));
            _names.Add(new TodoItem("dsadsadsa"));
            _names.Add(new TodoItem("dcftvgybuhnij"));

            MyListBox.ItemsSource = _names;
        }

        // From wpf tutorial
        public class TodoItem
        {
            public string Title { get; set; }

            public TodoItem(string s)
            {
                Title = s;
            }
        }

        private void MyListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lb = sender as ListBox;
            ListBoxItem lbi = lb.SelectedItem as ListBoxItem; // Casting



            Choice.Content = "Du valgte: " + lbi.Content;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _names.Add(new TodoItem("added later eksde"));
            MyListBox.ItemsSource = _names;
            MyListBox.Items.Refresh();
        }

        /*
        void PrintText(object sender, SelectionChangedEventArgs args)
        {
            ListBoxItem lbi = ((sender as ListBox).SelectedItem as ListBoxItem);
            tb.Text = "   You selected " + lbi.Content.ToString() + ".";
        }*/

    }
}
