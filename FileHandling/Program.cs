﻿using System;
using System.Collections.Generic;

namespace FileHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Testing out FileHandling library.\n\n");

            // Create new handler for file.
            FileHandler hGuestList = new FileHandler("hydacGuestList.txt");

            // just for us right now, need to make sure we start from a fresh.
            hGuestList.Delete();

            // Check if it exists.
            if (!hGuestList.Exists())
                hGuestList.Create();

            #region Now we can pretend we've gotten some informations about a new guest.
            // All wonderful, the information has been supplied to the system, we just need to save it.
            List<string> employeeNames = new List<string>();
            employeeNames.Add("Bill Gates");
            employeeNames.Add("Elon Musk");
            employeeNames.Add("Tim Apple");

            List<string> guestNames = new List<string>();
            guestNames.Add("Bob Taybor");
            guestNames.Add("Leif Teacher");
            guestNames.Add("Asger Teacher");

            List<string> roomNames = new List<string>();
            roomNames.Add("UCL");
            roomNames.Add("Microsoft");
            roomNames.Add("Mars");
            #endregion

            // Save the information in a list
            List<string> content = new List<string>();
            content.Add($"{employeeNames[1]},{guestNames[0]},{roomNames[2]}");
            content.Add($"{employeeNames[2]},{guestNames[1]},{roomNames[0]}");
            content.Add($"{employeeNames[0]},{guestNames[2]},{roomNames[1]}");

            // Store the meeting in the file. This is now being saved in a comma seperated format.
            hGuestList.Write(content);

            // Display content, in a proper format
            foreach(string s in hGuestList.GetAll())
            {
                string[] lC = s.Split(',');
                Console.WriteLine($"{lC[0]} har et møde med {lC[1]} i lokalet: {lC[2]}");
            }


            //// Initialize FileHandler
            //FileHandler fh = new FileHandler("test2.txt");

            //// Create file if it doesn't exist
            //if (!fh.Exists())
            //    fh.Create();

            //// Create content if empty
            //if(fh.Empty())
            //{
            //    // Write to the file
            //    fh.Write("List of waifus.\n");
            //    string[] waifus = { "Zero Two", "Yuno Gasai", "Darkness", "Akeno" };
            //    fh.Write(waifus);
            //}

            //// Read the entire file.
            ////fh.ReadAll();

            //// Get the content of the file.
            //List<string> contentOfFile = fh.GetAll();
            //foreach(string s in contentOfFile)
            //{
            //    Console.WriteLine("\t{0}", s);
            //}

            //// Read a specific line.
            //fh.ReadLine(3);
        }
    }
}
