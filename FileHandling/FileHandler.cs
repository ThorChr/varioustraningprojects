﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net.Mime;

namespace FileHandling
{
    public class FileHandler
    {
        // Declare properties
        public string PathToFile { get; set; }

        #region Constructor
        /// <summary>
        /// Gives properties required information.
        /// </summary>
        /// <param name="filePath">File this object shall focus on.</param>
        public FileHandler(string filePath)
        {
            PathToFile = filePath;
        }
        #endregion

        #region Exists() methods.
        /// <summary>
        /// Check if specified file exists.
        /// 
        /// Based on path given on initialization of object.
        /// </summary>
        /// <returns>True if file exists, false if not.</returns>
        public bool Exists()
        {
            // Checks existence
            bool exists = (File.Exists(PathToFile) ? true : false);
            return exists;
        }

        /// <summary>
        /// Check if specified file exists.
        /// If filePath argument is not supplied, PathToFile property will be used.
        /// </summary>
        /// <param name="filePath">Path to the required file. If not supplied, default will be used.</param>
        /// <returns>True if file exists, false if not.</returns>
        public bool Exists(string filePath)
        {
            // Checks existence
            bool exists = (File.Exists(filePath) ? true : false);
            return exists;
        }
        #endregion

        #region Create() methods.
        /// <summary>
        /// Creates a file at path given on object initialization.
        /// </summary>
        public void Create()
        {
            var myFile = File.Create(PathToFile);
            myFile.Close();
        }

        /// <summary>
        /// Creates a file at path.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public void Create(string path)
        {
            var myFile = File.Create(path);
            myFile.Close();
        }
        #endregion

        #region Empty() methods
        /// <summary>
        /// Check if file is empty or not. Path from object initialization
        /// </summary>
        /// <returns>True or false bool.</returns>
        public bool Empty()
        {
            // Get all content within the file.
            StreamReader r = new StreamReader(PathToFile);
            string file = r.ReadToEnd();
            r.Close();

            // Check the content
            return (new FileInfo(PathToFile).Length == 0 || file == "\r\n") ? true : false;
        }

        /// <summary>
        /// Check if file is empty or not. Path argument
        /// </summary>
        /// <returns>True or false bool.</returns>
        public bool Empty(string path)
        {
            // Get all content within the file.
            StreamReader r = new StreamReader(path);
            string file = r.ReadToEnd();
            r.Close();

            // Check the content
            return (new FileInfo(path).Length == 0 || file == "\r\n") ? true : false;
        }
        #endregion

        #region Clear() methods.
        /// <summary>
        /// Clears the entire file.
        /// </summary>
        public void Clear()
        {
            StreamWriter sw = new StreamWriter(PathToFile);
            sw.WriteLine("");
            sw.Close();
        }

        /// <summary>
        /// Clears the entire file.
        /// </summary>
        /// <param name="path">Path to specified file.</param>
        public void Clear(string path)
        {
            StreamWriter sw = new StreamWriter(path);
            sw.WriteLine("");
            sw.Close();
        }
        #endregion

        #region Write() methods.
        /// <summary>
        /// Writes to the specified file.
        /// </summary>
        /// <param name="input">String to be written to the file.</param>
        public void Write(string input)
        {
            if(Empty())
            {
                StreamWriter w = new StreamWriter(PathToFile);
                w.WriteLine(input);
                w.Close();
            } else
            {
                StreamWriter w = new StreamWriter(PathToFile, true);
                w.WriteLine(input);
                w.Close();
            }
        }

        /// <summary>
        /// Writes to the specified file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="input">String to be written to the file.</param>
        public void Write(string path, string input)
        {
            if (Empty())
            {
                StreamWriter w = new StreamWriter(path);
                w.WriteLine(input);
                w.Close();
            }
            else
            {
                StreamWriter w = new StreamWriter(path, true);
                w.WriteLine(input);
                w.Close();
            }
        }

        /// <summary>
        /// Writes to the specified file.
        /// </summary>
        /// <param name="input">String array to be written to the file.</param>
        public void Write(string[] input)
        {
            // Goes through all lines in the file.
            foreach(string i in input)
            {
                // Checks if the file is empty.
                // This is needed because we should replace all "content" if the file is "empty".
                // But if there already exists content, we should append to it.
                if(Empty())
                {
                    StreamWriter w = new StreamWriter(PathToFile);
                    w.WriteLine(i);
                    w.Close();
                } else
                {
                    StreamWriter w = new StreamWriter(PathToFile, true);
                    w.WriteLine(i);
                    w.Close();
                }
            }
        }

        /// <summary>
        /// Writes to the specified file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="input">String array to be written to the file.</param>
        public void Write(string path, string[] input)
        {
            // Goes through all lines in the file.
            foreach (string i in input)
            {
                // Checks if the file is empty.
                // This is needed because we should replace all "content" if the file is "empty".
                // But if there already exists content, we should append to it.
                if (Empty())
                {
                    StreamWriter w = new StreamWriter(path);
                    w.WriteLine(i);
                    w.Close();
                }
                else
                {
                    StreamWriter w = new StreamWriter(path, true);
                    w.WriteLine(i);
                    w.Close();
                }
            }
        }

        /// <summary>
        /// Writes to the specified file.
        /// </summary>
        /// <param name="input">String list to be written to the file.</param>
        public void Write(List<string> input)
        {
            // Goes through all lines in the file.
            foreach (string i in input)
            {
                // Checks if the file is empty.
                // This is needed because we should replace all "content" if the file is "empty".
                // But if there already exists content, we should append to it.
                if (Empty())
                {
                    StreamWriter w = new StreamWriter(PathToFile);
                    w.WriteLine(i);
                    w.Close();
                }
                else
                {
                    StreamWriter w = new StreamWriter(PathToFile, true);
                    w.WriteLine(i);
                    w.Close();
                }
            }
        }

        /// <summary>
        /// Writes to the specified file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="input">String list to be written to the file.</param>
        public void Write(string path, List<string> input)
        {
            // Goes through all lines in the file.
            foreach (string i in input)
            {
                // Checks if the file is empty.
                // This is needed because we should replace all "content" if the file is "empty".
                // But if there already exists content, we should append to it.
                if (Empty())
                {
                    StreamWriter w = new StreamWriter(path);
                    w.WriteLine(i);
                    w.Close();
                }
                else
                {
                    StreamWriter w = new StreamWriter(path, true);
                    w.WriteLine(i);
                    w.Close();
                }
            }
        }
        #endregion

        #region ReadAll() methods.
        /// <summary>
        /// Read all content from the file.
        /// </summary>
        public void ReadAll()
        {
            StreamReader r = new StreamReader(PathToFile);
            while(!r.EndOfStream)
            {
                string line = r.ReadLine();
                Console.WriteLine(line);
            }
            r.Close();
        }

        /// <summary>
        /// Read all content from the file at path.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public void ReadAll(string path)
        {
            StreamReader r = new StreamReader(path);
            while (!r.EndOfStream)
            {
                string line = r.ReadLine();
                Console.WriteLine(line);
            }
            r.Close();
        }
        #endregion

        #region ReadLine() methods.
        /// <summary>
        /// Reads a specific line from a text file.
        /// </summary>
        /// <param name="lineNumber">The required line.</param>
        public void ReadLine(int lineNumber)
        {
            try
            {
                StreamReader r = new StreamReader(PathToFile);

                // Create a for loop, to let us skip to the line we need.
                for (int i = 1; i < lineNumber; i++)
                {
                    r.ReadLine();

                    // If we reached end of the file, before getting to the requested line.
                    if (r.EndOfStream)
                    {
                        Console.WriteLine($"Error: File only contains {i} lines.");
                        break;
                    }
                }

                // Gucci, we're at the requested line, cw it.
                Console.WriteLine(r.ReadLine());

                r.Close();

            } catch (IOException e) { Console.WriteLine(e); }
        }

        /// <summary>
        /// Reads a specific line from a text file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="lineNumber">The required line.</param>
        public void ReadLine(string path, int lineNumber)
        {
            try
            {
                StreamReader r = new StreamReader(path);

                // Create a for loop, to let us skip to the line we need.
                for (int i = 1; i < lineNumber; i++)
                {
                    r.ReadLine();

                    // If we reached end of the file, before getting to the requested line.
                    if (r.EndOfStream)
                    {
                        Console.WriteLine($"Error: File only contains {i} lines.");
                        break;
                    }
                }

                // Gucci, we're at the requested line, cw it.
                Console.WriteLine(r.ReadLine());

                r.Close();

            } catch (IOException e) { Console.WriteLine(e); }
        }
        #endregion

        #region GetAll() methods.
        /// <summary>
        /// Read all content from the file.
        /// </summary>
        public List<string> GetAll()
        {
            List<string> content = new List<string>();

            // Get all content from the file.
            StreamReader r = new StreamReader(PathToFile);
            while (!r.EndOfStream)
            {
                string line = r.ReadLine();
                content.Add(line);
            }
            r.Close();

            // Return the content of the file
            return content;
        }

        /// <summary>
        /// Read all content from the file at path.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public List<string> GetAll(string path)
        {
            List<string> content = new List<string>();

            // Get all content from the file.
            StreamReader r = new StreamReader(path);
            while (!r.EndOfStream)
            {
                string line = r.ReadLine();
                content.Add(line);
            }
            r.Close();

            // Return the content of the file
            return content;
        }
        #endregion

        #region GetLine() methods.
        /// <summary>
        /// Returns a specific line from a text file.
        /// </summary>
        /// <param name="lineNumber">Wanted line.</param>
        /// <returns>String. Line from text file.</returns>
        public string GetLine(int lineNumber)
        {
            string content = "";

            try
            {
                StreamReader r = new StreamReader(PathToFile);

                // Create a for loop, to let us skip to the line we need.
                for (int i = 1; i < lineNumber; i++)
                {
                    r.ReadLine();

                    // If we reached end of the file, before getting to the requested line.
                    if (r.EndOfStream)
                    {
                        Console.WriteLine($"Error: File only contains {i} lines.");
                        break;
                    }
                }

                // Gucci, we're at the requested line, cw it.
                content = r.ReadLine();
                r.Close();

            } catch (IOException e) { Console.WriteLine(e); }
                
            return content;
        }

        /// <summary>
        /// Returns a specific line from a text file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="lineNumber">Wanted line.</param>
        /// <returns>String. Line from text file.</returns>
        public string GetLine(string path, int lineNumber)
        {
            string content = "";

            try
            {
                StreamReader r = new StreamReader(path);

                // Create a for loop, to let us skip to the line we need.
                for (int i = 1; i < lineNumber; i++)
                {
                    r.ReadLine();

                    // If we reached end of the file, before getting to the requested line.
                    if (r.EndOfStream)
                    {
                        Console.WriteLine($"Error: File only contains {i} lines.");
                        break;
                    }
                }

                // Gucci, we're at the requested line, cw it.
                content = r.ReadLine();
                r.Close();

            }
            catch (IOException e) { Console.WriteLine(e); }

            return content;
        }
        #endregion

        #region EditLine() methods.
        /// <summary>
        /// Edit a specific line in the text file.
        /// 
        /// Creates a new file based on new input. Then overwrites the original file with the new one.
        /// This secures data in case of PC shutdown happening in the process of writing to the file.
        /// </summary>
        /// <param name="content">The new content for the specified line.</param>
        /// <param name="lineNumber">The specified line number.</param>
        public void EditLine(string content, int lineNumber)
        {
            string tempFileName = "temporaryTextFileForEditingASingleLine.txt";

            // Create a new temp file.
            Create(tempFileName);

            // Read all content of old file.
            List<string> fileContent = GetAll();

            // Edit the specific line.
            fileContent[lineNumber - 1] = content;

            // Save updated list to temp file
            Write(tempFileName, fileContent);

            // Delete the original file.
            Delete();

            // Rename the temp file to original file name
            Rename(tempFileName, PathToFile);
        }

        /// <summary>
        /// Edit a specific line in the text file.
        /// 
        /// Creates a new file based on new input. Then overwrites the original file with the new one.
        /// This secures data in case of PC shutdown happening in the process of writing to the file.
        /// </summary>
        /// <param name="path">Path for the file.</param>
        /// <param name="content">The new content for the specified line.</param>
        /// <param name="lineNumber">The specified line number.</param>
        public void EditLine(string path, string content, int lineNumber)
        {
            string tempFileName = "temporaryTextFileForEditingASingleLine.txt";

            // Create a new temp file.
            Create(tempFileName);

            // Read all content of old file.
            List<string> fileContent = GetAll(path);

            // Edit the specific line.
            fileContent[lineNumber - 1] = content;

            // Save updated list to temp file
            Write(tempFileName, fileContent);

            // Delete the original file.
            Delete();

            // Rename the temp file to original file name
            Rename(tempFileName, path);
        }
        #endregion

        #region Delete() methods.
        /// <summary>
        /// Deleted file at path from property.
        /// </summary>
        public void Delete()
        {
            File.Delete(PathToFile);
        }

        /// <summary>
        /// Deletes file at path.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public void Delete(string path)
        {
            File.Delete(path);
        }
        #endregion

        #region DeleteLine() methods.
        /// <summary>
        /// Delete a specific line in the text file.
        /// 
        /// Creates a new file based on new input. Then overwrites the original file with the new one.
        /// This secures data in case of PC shutdown happening in the process of writing to the file.
        /// </summary>
        /// <param name="lineNumber">The specified line number.</param>
        public void DeleteLine(int lineNumber)
        {
            string tempFileName = "temporaryTextFileForDeletingASingleLine.txt";

            // Create a new temp file.
            Create(tempFileName);

            // Read all content of old file.
            List<string> fileContent = GetAll();

            // Delete the specific line.
            fileContent.RemoveAt(lineNumber-1);

            // Save updated list to temp file
            Write(tempFileName, fileContent);

            // Delete the original file.
            Delete();

            // Rename the temp file to original file name
            Rename(tempFileName, PathToFile);
        }

        /// <summary>
        /// Delete a specific line in the text file.
        /// 
        /// Creates a new file based on new input. Then overwrites the original file with the new one.
        /// This secures data in case of PC shutdown happening in the process of writing to the file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="lineNumber">The specified line number.</param>
        public void DeleteLine(string path, int lineNumber)
        {
            string tempFileName = "temporaryTextFileForDeletingASingleLine.txt";

            // Create a new temp file.
            Create(tempFileName);

            // Read all content of old file.
            List<string> fileContent = GetAll(path);

            // Delete the specific line.
            fileContent.RemoveAt(lineNumber - 1);

            // Save updated list to temp file
            Write(tempFileName, fileContent);

            // Delete the original file.
            Delete();

            // Rename the temp file to original file name
            Rename(tempFileName, path);
        }
        #endregion

        #region Rename() methods
        /// <summary>
        /// Renames the file.
        /// </summary>
        /// <param name="newName">New name of file (it's technically its full path).</param>
        public void Rename(string newName)
        {
            try
            {
                File.Move(PathToFile, newName);
                PathToFile = newName;
            } catch (IOException e) { Console.WriteLine(e); }
        }

        /// <summary>
        /// Renames the file.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="newName">New name of file (it's technically its full path).</param>
        public void Rename(string path, string newName)
        {
            try
            {
                File.Move(path, newName);
                PathToFile = newName;
            }
            catch (IOException e) { Console.WriteLine(e); }
        }
        #endregion
    }
}