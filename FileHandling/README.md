﻿# FileHandler
This class lowkey does what its name implies. 
It helps you handle text files.

With it, you can create textfiles, read them, get the content, edit specific lines and other stuff.

While some of the methods might be a bit redundant, because they also exist in the File class for C#, I still wrote them, to get some learning experience.

## Overview of the class
First of, the class requires a path given to it on initialization. However, there exists overloading of methods, so you can specify a different path if needed.

### Methods

#### Exists()
This is basically the File.Exists() method, but again, whatever. It's included for learning experiences. Returns a bool based on existence.

#### Create() 
Just as the Exists() method, it's just the File.Create() method.

#### Empty()
Returns true or false, dependent on whether or not the specified file is empty or not.
If no path is given, it'll use the path from the construction of the object.

#### Clear()
Clears the entire file. Doesn't delete it, only its content.

#### Write()
A lot of overloads exists for this one.

You can supply it with a single string, string array or string list. Then for each of those, you can also supply it with a custom path, or just let it use the default one.

#### ReadAll()
Read all content from file, and then writes it out to the Console.
Overloading also exists here, custom path or not.

#### ReadLine()
The same as ReadAll() just for a single line.

#### GetAll()
Read all content from file, then return it in a List\<string> format.

#### GetLine()
Same as GetAll(), just for a single line.

#### EditLine()
Edit a single line. Supply it with the new content and specified line number.
Overloading also exists here, custom path or no. But well, this is a pattern which exists all throughout the code.

#### Delete()
It's just straight up the File.Delete() method.

#### DeleteLine()
Deletes a single line. Code is based on the EditLine() but it deletes it, and not edits.

#### Rename()
Uses the File.Move() method, then also changes the PathToFile property to the new name.

## Ending notes
Could still use a lot of work e.g. make more use of try/catch. As of right now, it's very sparsely used. But whatev.