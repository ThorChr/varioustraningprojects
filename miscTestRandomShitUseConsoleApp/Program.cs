﻿using System;
using System.IO;

namespace miscTestRandomShitUseConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            bool endOfOrder = true;

            while(endOfOrder)
            {
                Console.Clear();

                // Need to read, so initiate new StreamReader object
                using (StreamReader sr = new StreamReader(@"MenuItems.txt"))
                {
                    string line;            // Going to hold value for each individual line (going line by line in loop, though text file)
                    string[] lineArray;     // Going to hold the split values from line.Split
                    int menuCounter = 1;

                    // As long as there are lines...
                    while((line = sr.ReadLine()) != null)
                    {
                        lineArray = line.Split(';');    // Split each line at every semi colon.
                        Console.WriteLine($"Første værdi i array: {lineArray[0]}");
                        Console.WriteLine($"Anden værdi i array: {lineArray[1]}");

                        menuCounter++;
                    }
                }
            }

        }
    }
}
