﻿using System;
using System.Collections.Generic;
using System.Text;

namespace classesTesting001
{
    class Person
    {
        // field to hold data
        public string Name;

        // method to do stuff
        public void Introduce(string to)
        {
            Console.WriteLine("sup {0}, my name is {1}", to, Name);
        }

        public static Person Parse(string str)
        {
            var person = new Person();
            person.Name = str;

            return person;
        }
    }
}
