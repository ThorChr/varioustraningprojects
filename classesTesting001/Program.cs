﻿using System;

namespace classesTesting001
{
    class Program
    {
        static void Main(string[] args)
        {
            // could also create a new instance of an object. 

            var person = Person.Parse("john");
            person.Introduce("eksde");
        }
    }
}
