﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using VideoCommandbind.ViewModels;

namespace VideoCommandbind.Commands
{
    public class UpdateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Returnerer bool baseret på om man kan execute eller ej.
        /// </summary>
        public bool CanExecute(object parameter)
        {
            // Vi vil altid have den her kan returneres, så vi siger bare true.
            // I et mere realistisk scenario, vil det ikke altid være true, fordi nogle gange vil man netop have der ikke er mulighed.
            // Fx, i Visual Studio kan man ikke kører en masse test relaterede ting, hvis man ikke er i et test projekt.
            // Samme logik her.
            return true;
        }



        public void Execute(object parameter)
        {
            // Tjekker om whatever object vi har fået tilsendt er det vi ønskede, altså vores mvm
            // hvis den condition returnerer true, bliver mvm tildelt en værdi, som svarer til parameter objektet, men konverteret til MainViewModel
            if(parameter is MainViewModel mvm)
            {
                mvm.Waifu = DateTime.Now.ToString();
            }
        }
    }
}
