﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using VideoCommandbind.Commands;

namespace VideoCommandbind.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string waifu = "002";

        public string Waifu
        {
            get { return waifu; }
            set { 
                waifu = value;
                OnPropertyChanged("Waifu");
                OnPropertyChanged("FullInfo");
            }
        }

        private string show = "DITF";

        public string Show
        {
            get { return show; }
            set { 
                show = value;
                OnPropertyChanged("Show");
                OnPropertyChanged("FullInfo");
            }
        }

        public string FullInfo
        {
            get { return $"{Waifu} [{Show}]"; }
        }

        // The command for CommandBinding on the XAML page
        public ICommand UpdateCommand { get; } = new UpdateCommand(); // Dvs. nu skal man lave en ny klasse der hedder UpdateCommand, der implementerer ICommand Interface

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
