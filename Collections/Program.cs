﻿using System;
using System.Collections.Generic; // Added by default. This makes sure we can use Lists.

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            // From collections video from Bob Taylor.
            Car car1 = new Car();
            car1.Make = "Audi";
            car1.Model = "A6";

            Car car2 = new Car();
            car2.Make = "Tesla";
            car2.Model = "Model S";

            Book b1 = new Book();
            b1.Author = "Carlo Zen";
            b1.Title = "The Saga of Tanya the Evil, Vol. 1 (light novel): Deus lo Vult";
            b1.ISBN = "0-316-51244-3";

            ///* -----------------
            // * List<T>
            // * T = Type.
            // * ----------------- */
            //List<Car> myList = new List<Car>();
            //myList.Add(car1);
            //myList.Add(car2);
            ////myList.Add(b1); No work, cuz wrong datatype.

            //foreach(Car c in myList)
            //{
            //    // No casting to make sure this works.
            //    // Because we told C# that this list, is of the datatype "Car".
            //    Console.WriteLine(c.Model);
            //}

            ///* -----------------
            // * Dictionary<TKey, TValue>
            // * TKey = Type of the keys in the dictionary.
            // * TValue = Type of the values in the dictionary.
            // * ----------------- */
            //Dictionary<string, Car> myDictionary = new Dictionary<string, Car>();
            //myDictionary.Add(car1.Make, car1);
            //myDictionary.Add(car2.Make, car2);

            //Console.WriteLine(myDictionary["Tesla"].Model);


        }
    }
}
