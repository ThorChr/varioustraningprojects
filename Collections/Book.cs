﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
    }
}
